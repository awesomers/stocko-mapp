package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;

public class ViewProfileRequest implements UserRequest {
    private String sessionToken;

    public ViewProfileRequest(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public JSONObject getJson() {
        JSONObject requestJson = new JSONObject();
        try {
            requestJson.put("page",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return requestJson;

    }

    public String getSessionToken() {
        return sessionToken;
    }
}
