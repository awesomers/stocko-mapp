package com.thoughtworks.stocko.domain;

import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: kishorek
 * Date: 07/09/12
 * Time: 9:15 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UserRequest {
    JSONObject getJson();
}
