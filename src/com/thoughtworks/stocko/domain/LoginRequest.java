package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginRequest implements UserRequest {

    private String email;
    private String password;

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("email",email);
            json.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }
}
