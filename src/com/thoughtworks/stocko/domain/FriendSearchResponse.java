package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FriendSearchResponse implements Response {
    private Friend[] friends;
    private boolean success;

    @Override
    public void setParameters(APIResponse apiResponse) {
        success = apiResponse.getStatus().equals("200");
        ArrayList<Friend> allFriends = new ArrayList<Friend>();
        if (success)
            try {
                JSONObject jsonResponse = new JSONObject(apiResponse.getJsonResponse());
                JSONArray friends = jsonResponse.getJSONArray("friends");
                for (int i = 0; i < friends.length(); i++) {
                    JSONObject friend = friends.getJSONObject(i);
                    String name = friend.get("Name").toString();
                    String email = friend.get("Email").toString();
                    String improvement = friend.get("Improvement").toString();

                    allFriends.add(new Friend(name, email,improvement));
                }
                this.friends = allFriends.toArray(new Friend[]{});
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public String[] getErrors() {
        return new String[0];
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

    public Friend[] getFriends() {
        return friends;
    }
}
