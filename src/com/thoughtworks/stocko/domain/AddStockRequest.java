package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AddStockRequest implements UserRequest {
    private String sessionToken;
    private final String shareCode;
    private final String quantity;
    private final String dateOfPurchase;
    private final String costPerShare;

    public AddStockRequest(String sessionToken, String shareCode, String quantity, String dateOfPurchase,
                           String costPerShare) {
        this.sessionToken = sessionToken;
        this.shareCode = shareCode;
        this.quantity = quantity;
        this.dateOfPurchase = dateOfPurchase;
        this.costPerShare = costPerShare;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();

        try {
            json.put("ShareSymbol",shareCode);
            json.put("Quantity",quantity);
            json.put("CostPerShare",costPerShare);
            json.put("DateOfPurchase",dateOfPurchase);
            json.put("LatestPrice","");
        } catch (JSONException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, e.toString());
            e.printStackTrace();
        }

        return json;

    }

    public String getSessionToken() {
        return sessionToken;
    }
}
