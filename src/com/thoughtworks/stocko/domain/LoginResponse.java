package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginResponse implements Response {
    private String sessionToken;
    private boolean success;

    public String getSessionToken() {
        return sessionToken;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setParameters(APIResponse apiResponse) {
        success = apiResponse.getStatus().equals("200");
        if(success)
            try {
                sessionToken = new JSONObject(apiResponse.getJsonResponse()).get("SuccessSessionToken").toString().trim();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public String[] getErrors() {
        return new String[0];  //To change body of implemented methods use File | Settings | File Templates.
    }
}
