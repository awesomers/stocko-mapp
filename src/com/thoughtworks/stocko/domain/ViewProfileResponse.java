package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewProfileResponse implements Response {
    private String name;
    private Stock[] stocks;
    private boolean success;

    @Override
    public void setParameters(APIResponse apiResponse) {
        success = apiResponse.getStatus().equals("200");
        ArrayList<Stock> allStocks = new ArrayList<Stock>();
        if (success)
            try {
                JSONObject jsonResponse = new JSONObject(apiResponse.getJsonResponse());
                name = jsonResponse.get("Name").toString();
                JSONArray shares = jsonResponse.getJSONArray("Stocks");
                for (int i = 0; i < shares.length(); i++) {
                    JSONObject share = shares.getJSONObject(i);
                    String shareName = share.get("ShareSymbol").toString();
                    float costPerShare = Float.parseFloat(share.get("CostPerShare").toString());
                    String purchaseDate = share.get("DateOfPurchase").toString();
                    String quantity = share.get("Quantity").toString();
                    allStocks.add(new Stock(shareName, costPerShare, purchaseDate, quantity));
                }
                stocks = allStocks.toArray(new Stock[]{});
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public String[] getErrors() {
        return new String[0];
    }

    public String getName() {
        return name;
    }

    public Stock[] getStocks() {
        return stocks;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }
}
