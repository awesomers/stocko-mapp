package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;

public interface Response {
    void setParameters(APIResponse apiResponse);
    String[] getErrors();
    boolean isSuccess();
}
