package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterRequest implements UserRequest {
    private final String name;
    private final String email;
    private final String password;

    public RegisterRequest(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    @Override
    public JSONObject getJson() {

        JSONObject json = new JSONObject();

        try {
            json.put("name",name);
            json.put("email",email);
            json.put("password",password);
        } catch (JSONException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, e.toString());
            e.printStackTrace();
        }

        return json;
    }
}
