package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FriendSearchRequest implements UserRequest {
    private String sessionToken;
    private String friend;

    public FriendSearchRequest(String sessionToken,String friend) {
        this.sessionToken = sessionToken;
        this.friend = friend;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();

        try {
            json.put("friend",friend);
        } catch (JSONException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, e.toString());
            e.printStackTrace();
        }

        return json;

    }

    public String getSessionToken() {
        return sessionToken;
    }
    public String getFriend() {
        return friend;
    }
}
