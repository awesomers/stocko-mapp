package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterResponse implements Response {
    private boolean success;
    private String sessionToken;

    public boolean isSuccess() {
        return success;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSuccess(String status) {
        success = status.equals("201");
    }

    public void setSessionToken(Object sessionId) {
        sessionToken = sessionId.toString();
    }

    @Override
    public void setParameters(APIResponse apiResponse) {
        setSuccess(apiResponse.getStatus());
        if(isSuccess())
        try {
            setSessionToken(new JSONObject(apiResponse.getJsonResponse()).get("SuccessSessionToken"));
        } catch (JSONException e) {
            setSessionToken(null);
        }
    }

    @Override
    public String[] getErrors() {
        return new String[0];  //To change body of implemented methods use File | Settings | File Templates.
    }
}
