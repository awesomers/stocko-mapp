package com.thoughtworks.stocko.domain;

public class Friend {
    private  String name;
    private  String email;
    private  String improvement;

    public Friend(String Name, String Email, String Improvement){

        name = Name;
        email = Email;
        improvement = Improvement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImprovement() {
        return improvement;
    }

    public void setImprovement(String improvement) {
        this.improvement = improvement;
    }
}
