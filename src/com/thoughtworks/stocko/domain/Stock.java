package com.thoughtworks.stocko.domain;

public class Stock {
    private final String ShareSymbol;
    private final double CostPerShare;
    private String DateOfPurchase;
    private String quantity;

    public String getShareSymbol() {
        return ShareSymbol;
    }

    public double getCostPerShare() {
        return CostPerShare;
    }

    public String getDateOfPurchase() {
        return DateOfPurchase;
    }

    public String getQuantity() {
        return quantity;
    }

    public Stock(String ShareSymbol, double CostPerShare, String DateOfPurchase, String quantity) {
        this.ShareSymbol = ShareSymbol;
        this.CostPerShare = CostPerShare;
        this.DateOfPurchase = DateOfPurchase;
        this.quantity = quantity;
    }

    public boolean equals(Stock stock) {

        if (Double.compare(stock.CostPerShare, CostPerShare) != 0) return false;
        if (DateOfPurchase != null ? !DateOfPurchase.equals(stock.DateOfPurchase) : stock.DateOfPurchase != null) return false;
        if (ShareSymbol != null ? !ShareSymbol.equals(stock.ShareSymbol) : stock.ShareSymbol != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "ShareSymbol='" + ShareSymbol + '\'' +
                ", CostPerShare=" + CostPerShare +
                ", DateOfPurchase='" + DateOfPurchase + '\'' +
                '}';
    }
}
