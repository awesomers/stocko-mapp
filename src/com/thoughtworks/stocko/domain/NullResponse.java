package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;

public class NullResponse implements Response {
    @Override
    public void setParameters(APIResponse apiResponse) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String[] getErrors() {
        return new String[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSuccess() {
        return false;
    }
}
