package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

public class FirstTimeActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_time);
    }

    public void continueToProfile(View view){
        setFirstTimeFalse();
        finish();
    }
    private void setFirstTimeFalse() {
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("FirstTime","false");
        editor.commit();
    }

}
