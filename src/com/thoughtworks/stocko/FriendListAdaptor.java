package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thoughtworks.stocko.domain.Friend;

public class FriendListAdaptor extends BaseAdapter {

    private final Activity activity;
    private final Friend[] friends;
    private LayoutInflater inflater;

    public FriendListAdaptor(Activity activity, Friend[] friends) {
        this.activity = activity;
        this.friends = friends;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return friends.length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getItem(int i) {
        return friends[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int i) {
        return i;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.friend_view, null);

        TextView friendName = (TextView) vi.findViewById(R.id.friend_name);
        TextView email = (TextView) vi.findViewById(R.id.friend_email);

        friendName.setText(friends[i].getName());
        email.setText(friends[i].getEmail() + "");
        return vi;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
