package com.thoughtworks.stocko;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.*;


public class LoginActivity extends Activity {

    private Gateway gateway;
    private final Handler handler;

    public LoginActivity() {
        gateway = new Gateway(new Communicator());
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
    }

    public void navigateToRegisterView(View view) {
        Intent registerIntent = new Intent(this, UserRegisterActivity.class);
        startActivity(registerIntent);
        overridePendingTransition(R.anim.slide_to_top, R.anim.slide_away_top);
        finish();
    }

    public void login(View view) {
        UserRequest request = createLoginRequest();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(!connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting()){
            Toast.makeText(this,R.string.err_no_connection, Toast.LENGTH_LONG).show();
            return;
        }

        if (request instanceof LoginRequest) {
            ProgressBar progress = (ProgressBar) findViewById(R.id.login_progress);
            progress.setVisibility(View.VISIBLE);
            navigateToHome(request);
        }

    }

    private void navigateToHome(UserRequest request) {
        final UserRequest userRequest = request;
        final Context uiContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Response response = gateway.sendRequest(userRequest);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (response instanceof LoginResponse) {
                            LoginResponse loginResponse = (LoginResponse) response;
                            if (loginResponse.isSuccess()) {
                                Toast.makeText(uiContext, R.string.msg_login_success, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(uiContext, UserHomeActivity.class);
                                saveSessionToken(loginResponse.getSessionToken());
                                intent.putExtra(StockOConstants.SESSION_TOKEN, loginResponse.getSessionToken());
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_away_top);
                                finish();
                            } else {
                                Toast.makeText(uiContext, R.string.err_login_failed, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(uiContext, "An Error has occured!!", Toast.LENGTH_LONG);
                        }
                        ProgressBar progress = (ProgressBar) findViewById(R.id.login_progress);
                        progress.setVisibility(View.GONE);
                    }
                });

            }
        }).start();

    }

    private void saveSessionToken(String sessionToken) {
        SharedPreferences userData = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor editor = userData.edit();
        editor.putString("sessionToken", sessionToken);
        editor.commit();
    }

    private UserRequest createLoginRequest() {
        String email = getViewText(R.id.email);
        String password = getViewText(R.id.password);
        if (email.isEmpty()) {
            showErrorMessage(R.string.err_invalid_email);
            return new InvalidInputError();
        }
        if (password.isEmpty()) {
            showErrorMessage(R.string.err_no_password);
            return new InvalidInputError();
        }
        return new LoginRequest(email, password);
    }

    private void showErrorMessage(int msgId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder.setMessage(msgId).setNeutralButton("Ok", null).create();
        alertDialog.show();
    }

    private String getViewText(int viewId) {
        return ((EditText) findViewById(viewId)).getText().toString().trim();
    }
}

