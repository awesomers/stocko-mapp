package com.thoughtworks.stocko.apiaccess;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class NetworkTask extends AsyncTask<String, String, HttpResponse> {

    private  String url;
    private final String content;
    private String sessionId;
    private String getParameter;
    private HttpUriRequest httpUriRequest;

    public NetworkTask(RequestType requestType, String content, String url, String sessionId, String getParameter) {
        this.url = url;
        this.content = content;
        this.sessionId = sessionId;
        this.getParameter = getParameter;
        if(requestType.equals(RequestType.HTTPPOST)){
            setPostRequest();
        }else{
            setGetRequest();
        }
    }

    private void setGetRequest() {
        if(getParameter != null)
        url= url+getParameter;
        httpUriRequest = new HttpGet(url);
    }

    private void setPostRequest() {
        HttpPost httpPost = new HttpPost(url);
        try {
            StringEntity stringEntity = new StringEntity(content);
            stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPost.setEntity(stringEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        httpUriRequest = httpPost;
    }

    @Override
    protected HttpResponse doInBackground(String... params) {
        try {
            Log.d("NetworkTask", "Sending post response "+ httpUriRequest.toString());
            DefaultHttpClient httpClient  = new DefaultHttpClient();
            if(sessionId != null)
            httpUriRequest.setHeader("sessionId", sessionId);
            HttpResponse response = httpClient.execute(httpUriRequest);
            return response;
        } catch (IOException e){
            Log.d("NetworkTask", "Exception : "+e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
