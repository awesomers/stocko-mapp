package com.thoughtworks.stocko.apiaccess;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

public class Communicator {

    private HttpResponse httpResponse;

    public APIResponse send(RequestType requestMethod, String requestJson, String apiUrl, String sessionId, String getParameter) {

        NetworkTask networkTask = new NetworkTask(requestMethod, requestJson, apiUrl, sessionId, getParameter);
        try {
            httpResponse = networkTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        return (httpResponse != null)? constructApiResponse(httpResponse) : new APIResponse();
    }

    private APIResponse constructApiResponse(HttpResponse httpResponse) {

        try {
            HttpEntity entity = httpResponse.getEntity();
            InputStream inputStream = entity.getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String responseText = null;
            responseText = bufferedReader.readLine();
            APIResponse apiResponse = new APIResponse();
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            apiResponse.setStatus(Integer.toString(statusCode));
            apiResponse.setResponseText(responseText);


            return apiResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private APIResponse mockedResponse(){
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("200");
        apiResponse.setResponseText("{SuccessSessionToken: sesas8o4asjdasvlasv}");
        return apiResponse;
    }
}
