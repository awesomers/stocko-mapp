package com.thoughtworks.stocko.apiaccess;

public class APIResponse {
    private String[] errors;
    private String status;
    private String jsonResponse;
    public String Header;

    public APIResponse() {
        this.status = "FAILED";
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponseText(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    public String getJsonResponse() {
        return jsonResponse;
    }

    public String getStatus() {
        return status;
    }

    public String[] getErrors(){
        return errors;
    }
}
