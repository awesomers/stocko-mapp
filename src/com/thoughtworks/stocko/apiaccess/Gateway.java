package com.thoughtworks.stocko.apiaccess;

import com.thoughtworks.stocko.domain.*;
import org.json.JSONObject;

public class Gateway {
    private Communicator communicator;

    public Gateway(Communicator communicator) {
        this.communicator = communicator;
    }

    public Response sendRequest(UserRequest request)  {
        JSONObject requestJson = request.getJson();
        APIResponse apiResponse;
        String apiUrl = "";
        String requestParameter = null;

        RequestType requestType = RequestType.NONE;
        Response response = new NullResponse();
        String sessionToken=null;

        if(request instanceof RegisterRequest){
            apiUrl = StockOConstants.REGISTER_API_URL;
            requestType = RequestType.HTTPPOST;
            response = new RegisterResponse();
        }
        if(request instanceof LoginRequest) {
            apiUrl = StockOConstants.LOGIN_API_URL;
            requestType = RequestType.HTTPPOST;
            response = new LoginResponse();
        }
        if(request instanceof ViewProfileRequest){
            apiUrl = StockOConstants.VIEW_PROFILE_API_URL;
            requestType = RequestType.HTTPGET;
            sessionToken = ((ViewProfileRequest) request).getSessionToken();
            response = new ViewProfileResponse();
        }
        if(request instanceof AddStockRequest){
            apiUrl = StockOConstants.ADD_STOCK_API_URL;
            requestType = RequestType.HTTPPOST;
            sessionToken = ((AddStockRequest) request).getSessionToken();
            response = new ViewProfileResponse();
        }
        if(request instanceof FriendSearchRequest){
            apiUrl = StockOConstants.SEARCH_FRIENDS_API_URL;
            requestType = RequestType.HTTPGET;
            sessionToken = ((FriendSearchRequest) request).getSessionToken();
            requestParameter = ((FriendSearchRequest) request).getFriend();
            response = new FriendSearchResponse();

        }

        apiResponse = communicator.send(requestType, requestJson==null ? null: requestJson.toString(), apiUrl, sessionToken, requestParameter);
        response.setParameters(apiResponse);
        return response;
    }
}
