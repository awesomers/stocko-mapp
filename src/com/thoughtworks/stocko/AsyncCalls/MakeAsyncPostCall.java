package com.thoughtworks.stocko.asynccalls;

import android.os.AsyncTask;
import android.widget.TextView;
import com.thoughtworks.stocko.R;
import com.thoughtworks.stocko.domain.StockOConstants;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;

public class MakeAsyncPostCall extends AsyncTask<String, String, String> {

        private TextView textView;

        public MakeAsyncPostCall(TextView textView) {
            this.textView = textView;
        }

        @Override
        protected String doInBackground(String... strings) {
            return sendMessage();
        }

        private String sendMessage() {
            HttpClient httpclient = new DefaultHttpClient();
            String statusCode;

            try {
                HttpPost httpPost = new HttpPost(String.format("http://%s:%s/api/registration", R.string.host_name, R.string.port));
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("Name",""));
                nameValuePairs.add(new BasicNameValuePair("Email","kool@kool.com"));
                nameValuePairs.add(new BasicNameValuePair("Password","password"));
                nameValuePairs.add(new BasicNameValuePair("apikey", StockOConstants.API_KEY));

                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpclient.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();
                statusCode = "The Return Status Code from REST API is: "+statusLine.getStatusCode();
            } catch (IOException e) {
                statusCode ="IO exception: "+ e.toString();  //To change body of catch statement use File | Settings | File Templates.
            }

            return statusCode;
        }

        @Override
        protected void onPostExecute(String s) {
            textView.setText(s);
        }
}
