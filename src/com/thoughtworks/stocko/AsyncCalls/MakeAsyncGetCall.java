package com.thoughtworks.stocko.asynccalls;

import android.os.AsyncTask;
import android.widget.TextView;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created with IntelliJ IDEA.
 * User: vishnuk
 * Date: 08/09/12
 * Time: 13:43
 * To change this template use File | Settings | File Templates.
 */
public class MakeAsyncGetCall extends AsyncTask<String, String, String> {

    private TextView textView;

    public MakeAsyncGetCall(TextView textView) {
        this.textView = textView;
    }

    @Override
    protected String doInBackground(String... strings) {
        return sendMessage();
    }

    private String sendMessage() {
        HttpClient httpclient = new DefaultHttpClient();
        String statusCode;
        try {
            //            HttpResponse response = httpclient.execute(new HttpGet("http://10.0.2.2/stocko/api/profile?page=1"));
//            StatusLine statusLine = response.getStatusLine();

            URL url = new URL("http://10.0.2.2/stocko/api/profile?page=1");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("sessionId","this is session ID");
            BufferedInputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            String statusLine = inputStream.toString();
            statusCode = "The Return Status Code from REST API: "+statusLine;


        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            statusCode ="MalformedURLException: "+ e.toString();  //To change body of catch statement use File | Settings | File Templates.

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            statusCode ="IO exception: "+ e.toString();  //To change body of catch statement use File | Settings | File Templates.

        }


        return statusCode;
    }

    @Override
    protected void onPostExecute(String s) {
        textView.setText(s);
    }
}
