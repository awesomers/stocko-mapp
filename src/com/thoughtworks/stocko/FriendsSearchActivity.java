package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.*;

public class FriendsSearchActivity extends Activity {

    private String sessionToken;
    private final Gateway gateway;
    private final Handler handler;

    public FriendsSearchActivity() {
        gateway = new Gateway(new Communicator());
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_search);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        SharedPreferences userData = getSharedPreferences("userData", MODE_PRIVATE);
        sessionToken = userData.getString("sessionToken", null);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        backToHomeFromFriends(null);
    }


    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void backToHomeFromFriends(View view){
        Intent homeIntent = new Intent(this, UserHomeActivity.class);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_away_top,R.anim.slide_to_top);
        finish();
    }

    public void searchFriend(View view){

        new Thread(new Runnable() {

            @Override
            public void run() {


        EditText viewById = (EditText)findViewById(R.id.friend_search_text);
        String text = viewById.getText().toString();
        final FriendSearchResponse friendSearchResponse = (FriendSearchResponse)gateway
                .sendRequest(new FriendSearchRequest(sessionToken, text));

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (friendSearchResponse.isSuccess()){

                            updateFriendsView(friendSearchResponse);

                    }}
                });
            }
        }).start();
    }

                    private void updateFriendsView(FriendSearchResponse friendSearchResponse) {
                        ListView friendList = (ListView) findViewById(R.id.friendList);
                        friendList.setAdapter(new FriendListAdaptor(this, friendSearchResponse.getFriends()));
                    }
    public void doNothing(View view) {

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//            }}
//        ).start();
        }
}
