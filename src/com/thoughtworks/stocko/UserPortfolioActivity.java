package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.ViewProfileRequest;
import com.thoughtworks.stocko.domain.ViewProfileResponse;

public class UserPortfolioActivity extends Activity {

    private String sessionToken;
    private final Gateway gateway;
    private final Handler handler;

    public UserPortfolioActivity() {
        gateway = new Gateway(new Communicator());
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_pfolio);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        SharedPreferences userData = getSharedPreferences("userData", MODE_PRIVATE);
        sessionToken = userData.getString("sessionToken", null);
        String isFirstTime = userData.getString("FirstTime", null);
        if(isFirstTime == "true"){
            showFirstTimeActivity();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        backToHome(null);
    }

    private void showFirstTimeActivity() {
        Intent intent = new Intent(this, FirstTimeActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onResume(){
        super.onResume();
        getProfileData();

    }

    private void getProfileData() {
        final Context uiContext = this;
        final ProgressBar loadingProgress = (ProgressBar) findViewById(R.id.loadingPortfolio);
        loadingProgress.setVisibility(View.VISIBLE);


        new Thread(new Runnable() {

            @Override
            public void run() {
                final ViewProfileResponse viewProfileResponse = (ViewProfileResponse) gateway
                        .sendRequest(new ViewProfileRequest(sessionToken));
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(viewProfileResponse.isSuccess()){
                            TextView viewById = (TextView) findViewById(R.id.welcome_profile_msg);
                            viewById.setText("Hello " + viewProfileResponse.getName());

                            updateProfileView(viewProfileResponse);

                        }else{
                            Toast.makeText(uiContext,"An Error Occurred!", Toast.LENGTH_LONG).show();
                        }

                        loadingProgress.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }


    private void updateProfileView(ViewProfileResponse viewProfileResponse) {
        ListView stockList = (ListView) findViewById(R.id.stocklist);
        stockList.setAdapter(new StockListAdaptor(this, viewProfileResponse.getStocks()));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProfileData();

    }

    public void addToPortfolio(View view){
        Intent addStockIntent = new Intent(this, UserAddStockActivity.class);
        startActivity(addStockIntent);
        overridePendingTransition(R.anim.slide_to_top,R.anim.slide_away_top);
    }

    public void backToHome(View view){
        Intent homeIntent = new Intent(this, UserHomeActivity.class);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_away_top,R.anim.slide_to_top);
        finish();
    }


}
