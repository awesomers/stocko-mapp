package com.thoughtworks.stocko;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.*;

public class UserRegisterActivity extends Activity {

    private final Gateway gateway;
    private final Handler handler;

    public UserRegisterActivity() {
        gateway = new Gateway(new Communicator());
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register);
    }

    public void registerUser(View view){
        UserRequest registerRequest = createRegisterRequest();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(!connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting()){
            Toast.makeText(this,R.string.err_no_connection, Toast.LENGTH_LONG).show();
            return;
        }

        if(registerRequest instanceof RegisterRequest){
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.register_progress);
            progressBar.setVisibility(View.VISIBLE);
            navigateToNextPage(registerRequest);
        }

    }

    private void navigateToNextPage(UserRequest registerRequest) {
        final UserRequest request = registerRequest;
        final Context uiContext = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Response response = gateway.sendRequest(request);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(response instanceof RegisterResponse){
                            RegisterResponse registerResponse = (RegisterResponse) response;
                            if(registerResponse.isSuccess()){
                                Toast.makeText(uiContext, R.string.register_success, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(uiContext, UserPortfolioActivity.class);
                                intent.putExtra(StockOConstants.SESSION_TOKEN, registerResponse.getSessionToken());
//                                intent.putExtra("FirstTime",true);
                                setFirstTimeTrue();
                                saveSessionToken(registerResponse.getSessionToken());
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_to_top,R.anim.slide_away_top);
                                finish();
                            }else{
                                showErrorMessage(R.string.err_reg_failed_user_exist);
                            }
                        } else {
                            showErrorMessage(R.string.err_generic);
                        }
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.register_progress);
                        progressBar.setVisibility(View.GONE);
                    }
                });

            }
        }).start();


    }

    private void setFirstTimeTrue() {
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("FirstTime","true");
        editor.commit();
    }

    private void saveSessionToken(String sessionToken) {
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("sessionToken",sessionToken);
        editor.commit();
    }

    private UserRequest createRegisterRequest() {
        String name = getViewText(R.id.name);
        String email = getViewText(R.id.email);
        String password = getViewText(R.id.password);
        String verifyPassword = getViewText(R.id.verify);

        if(name.isEmpty()){
            showErrorMessage(R.string.err_no_name);
            return new InvalidInputError();
        }
        if(email.isEmpty() || !email.matches("[a-zA_Z][a-zA-Z_.]*[@][a-zA-Z][a-zA-Z]*[.][a-zA-Z][a-zA-Z]*[a-zA-Z.]*")){
            showErrorMessage(R.string.err_invalid_email);
            return new InvalidInputError();
        }
        if(password.isEmpty()){
            showErrorMessage(R.string.err_no_password);
            return new InvalidInputError();
        }
        if(!password.equals(verifyPassword)){
            showErrorMessage(R.string.err_password_mismatch);
            return new InvalidInputError();
        }

        return new RegisterRequest(name,email,password);
    }

    private void showErrorMessage(int msgId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder.setMessage(msgId).setNeutralButton("Ok",null).create();
        alertDialog.show();
    }


    private String getViewText(int viewId) {
        return ((EditText) findViewById(viewId)).getText().toString();
    }
}
