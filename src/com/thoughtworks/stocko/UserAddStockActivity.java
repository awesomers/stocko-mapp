package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.AddStockRequest;
import com.thoughtworks.stocko.domain.Response;
import com.thoughtworks.stocko.domain.ViewProfileRequest;
import com.thoughtworks.stocko.domain.ViewProfileResponse;

public class UserAddStockActivity extends Activity{
    private Gateway gateway;
    private String sessionToken;

    public UserAddStockActivity(){
        gateway = new Gateway(new Communicator());
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_stock);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
//        sessionToken = extras.getString(StockOConstants.SESSION_TOKEN);

        SharedPreferences userData = getSharedPreferences("userData", MODE_PRIVATE);
        sessionToken = userData.getString("sessionToken", null);
        }


    public void backToPortfolio(View view){
        Intent portfolioIntent = new Intent(this, UserPortfolioActivity.class);
        startActivity(portfolioIntent);
        overridePendingTransition(R.anim.slide_away_top, R.anim.slide_to_top);
        finish();
    }

    public void addStock(View view){
        EditText viewById1 = (EditText)findViewById(R.id.stock_code);
        String code = viewById1.getText().toString();

        EditText viewById2 = (EditText)findViewById(R.id.stock_quantity);
        String quantity = viewById2.getText().toString();

        EditText viewById3 = (EditText)findViewById(R.id.stock_purchase_cost);
        String cost = viewById3.getText().toString();

        DatePicker viewById4 = (DatePicker)findViewById(R.id.stock_purchase_date);
        String date = viewById4.getYear() + "-"
                + viewById4.getMonth() + "-"
                +viewById4.getDayOfMonth();



        ViewProfileResponse response = (ViewProfileResponse)gateway
                .sendRequest(new AddStockRequest(sessionToken, code, quantity, date, cost));
        Intent portfolio = new Intent(this, UserPortfolioActivity.class);
        startActivity(portfolio);
    }

}
