package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.thoughtworks.stocko.domain.StockOConstants;

public class WelcomeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Intent registerIntent = getNextIntent();
        startActivity(registerIntent);
        overridePendingTransition(R.anim.slide_to_top,0);
        finish();
    }

    private Intent getNextIntent() {
        SharedPreferences userData = getSharedPreferences("userData", MODE_PRIVATE);
        String sessionToken = userData.getString("sessionToken", null);
        if(sessionToken != null){
            Intent homeIntent = new Intent(this, UserHomeActivity.class);
            homeIntent.putExtra(StockOConstants.SESSION_TOKEN, sessionToken);
            return homeIntent;
        }
        return new Intent(this, LoginActivity.class);
    }
    @Override
    public void onBackPressed(){
        System.exit(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
