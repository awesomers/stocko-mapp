package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.thoughtworks.stocko.apiaccess.Communicator;
import com.thoughtworks.stocko.apiaccess.Gateway;
import com.thoughtworks.stocko.domain.StockOConstants;

public class UserHomeActivity extends Activity {

    private String sessionToken;
    private final Gateway gateway;

    public UserHomeActivity() {
        gateway = new Gateway(new Communicator());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        sessionToken = extras.get(StockOConstants.SESSION_TOKEN).toString();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showPortfolio(View view){
        Intent portfolioIntent = new Intent(this, UserPortfolioActivity.class);
        portfolioIntent.putExtra(StockOConstants.SESSION_TOKEN, sessionToken);
        startActivity(portfolioIntent);
        overridePendingTransition(R.anim.slide_to_top,R.anim.slide_away_top);
        finish();
    }

    public void showFriendsSearch(View view){
        Intent friendsIntent = new Intent(this, FriendsSearchActivity.class);
        friendsIntent.putExtra(StockOConstants.SESSION_TOKEN, sessionToken);
        startActivity(friendsIntent);
        overridePendingTransition(R.anim.slide_to_top,R.anim.slide_away_top);
        finish();
    }


}
