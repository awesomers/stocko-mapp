package com.thoughtworks.stocko;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thoughtworks.stocko.domain.Stock;

public class StockListAdaptor extends BaseAdapter {

    private final Activity activity;
    private final Stock[] stocks;
    private LayoutInflater inflater;

    public StockListAdaptor(Activity activity, Stock[] stocks) {
        this.activity = activity;
        this.stocks = stocks;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return stocks.length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getItem(int i) {
        return stocks[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getItemId(int i) {
        return i;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.stock_view, null);

        TextView stockNameView = (TextView) vi.findViewById(R.id.stock_name);
        TextView priceView = (TextView) vi.findViewById(R.id.latest_price);
        TextView quantity = (TextView) vi.findViewById(R.id.quantity);

        stockNameView.setText(stocks[i].getShareSymbol());
        priceView.setText(stocks[i].getCostPerShare()+"");
        quantity.setText(stocks[i].getQuantity());
        return vi;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
