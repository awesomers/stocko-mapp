package com.thoughtworks.stocko.apiaccess;

import com.thoughtworks.stocko.domain.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GatewayTest {

    @Mock
    public Communicator communicator;

    @Before
    public void before(){
        initMocks(this);
    }

    @Test
    public void aPostRequestIsSentToTheRegisterAPIForARegisterRequest(){
        RegisterRequest registerRequest = new RegisterRequest("name", "email", "password");
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("SUCCESS");
        apiResponse.setResponseText("{token:thid1655sesonkeybl8hbdl8}");

        when(communicator.send(RequestType.HTTPPOST, registerRequest.getJson().toString(), StockOConstants.REGISTER_API_URL, null, null)).thenReturn(apiResponse);
        Gateway gateway = new Gateway(communicator);

        Response response = gateway.sendRequest(registerRequest);

        Assert.assertThat(response, is(instanceOf(RegisterResponse.class)));

    }

    @Test
    public void aPostRequestIsSentToLoginAPIForALoginRequest(){
        LoginRequest loginRequest = new LoginRequest("email@domain.com", "password");
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("SUCCESS");
        apiResponse.setResponseText("{token:thid1655sesonkeybl8hbdl8}");

        when(communicator.send(RequestType.HTTPPOST, loginRequest.getJson().toString(), StockOConstants.LOGIN_API_URL, null, null)).thenReturn(apiResponse);
        Gateway gateway = new Gateway(communicator);

        Response response = gateway.sendRequest(loginRequest);
        Assert.assertThat(response, is(instanceOf(LoginResponse.class)));

    }

    @Test
    public void aGetRequestIsSentToViewProfileAPIForAViewProfileRequest(){
        ViewProfileRequest request = new ViewProfileRequest("thisIs4a553i0nt0ken");

        when(communicator.send(RequestType.HTTPGET, request.getJson().toString(), StockOConstants.VIEW_PROFILE_API_URL, null, null)).thenReturn(new APIResponse());
        Gateway gateway = new Gateway(communicator);

        Response response = gateway.sendRequest(request);
        Assert.assertThat(response, is(instanceOf(ViewProfileResponse.class)));
    }


}
