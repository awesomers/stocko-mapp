package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

public class LoginResponseTest {
    @Test
    public void constructTheLoginResponseUsingTheGivenApiResponse(){
        LoginResponse loginResponse = new LoginResponse();
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("SUCCESS");
        apiResponse.setResponseText("{token:675asdfas649w7awfawhf}");
        loginResponse.setParameters(apiResponse);

        Assert.assertThat(loginResponse.isSuccess(), is(true));
        Assert.assertThat(loginResponse.getSessionToken(), is("675asdfas649w7awfawhf"));
    }

    @Test
    public void constructAFailureLoginResponseWhenApiResponseIsAFailureResponse(){
        LoginResponse loginResponse = new LoginResponse();
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("FAILED");

        loginResponse.setParameters(apiResponse);

        Assert.assertThat(loginResponse.isSuccess(), is(false));
        Assert.assertThat(loginResponse.getSessionToken(), is(nullValue()));

    }
}
