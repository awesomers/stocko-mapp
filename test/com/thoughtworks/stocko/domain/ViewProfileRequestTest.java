package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class ViewProfileRequestTest {
    @Test
    public void theEquivalentJsonStringIsReturned() throws JSONException {
        ViewProfileRequest request = new ViewProfileRequest("thisIs24asessionT043dn");
        JSONObject requestJson = request.getJson();
        JSONObject json = new JSONObject("{sessionToken: thisIs24asessionT043dn}");
        Assert.assertThat(requestJson.toString(), is(json.toString()));

    }
}
