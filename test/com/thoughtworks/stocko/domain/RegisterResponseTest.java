package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

public class RegisterResponseTest {
    @Test
    public void constructTheRegisterResponseUsingTheGivenApiResponse(){
        RegisterResponse registerResponse = new RegisterResponse();
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("SUCCESS");
        apiResponse.setResponseText("{token:675asdfas649w7awfawhf}");
        registerResponse.setParameters(apiResponse);

        Assert.assertThat(registerResponse.isSuccess(), is(true));
        Assert.assertThat(registerResponse.getSessionToken(), is("675asdfas649w7awfawhf"));
    }

    @Test
    public void constructAFailureRegisterResponseWhenApiResponseIsAFailureResponse(){
        RegisterResponse registerResponse = new RegisterResponse();
        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("FAILED");

        registerResponse.setParameters(apiResponse);

        Assert.assertThat(registerResponse.isSuccess(), is(false));
        Assert.assertThat(registerResponse.getSessionToken(), is(nullValue()));

    }

}
