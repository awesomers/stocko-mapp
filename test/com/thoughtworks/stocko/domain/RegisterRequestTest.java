package com.thoughtworks.stocko.domain;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created with IntelliJ IDEA.
 * User: kishorek
 * Date: 15/09/12
 * Time: 4:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class RegisterRequestTest {

    @Test
    public void theEquivalentJsonStringIsReturned() throws JSONException {
        RegisterRequest registerRequest = new RegisterRequest("myname", "myemail@domain.com", "password");
        JSONObject registerRequestJson = registerRequest.getJson();

        JSONObject requestJson = new JSONObject();
        requestJson.put("name","myname");
        requestJson.put("email","myemail@domain.com");
        requestJson.put("password","password");

        Assert.assertThat(registerRequestJson.toString(), is(requestJson.toString()));
    }

}
