package com.thoughtworks.stocko.domain;

import com.thoughtworks.stocko.apiaccess.APIResponse;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class ViewProfileResponseTest {
    @Test
    public void constructViewProfileResponseUsingTheGivenSuccessApiResponse() throws JSONException {
        String jsonResponse = "{name: Dude, shares: [{shareName: Infy, price: 23.45, purchaseDate: 11-12-12},{shareName: TW, price: 43.45, purchaseDate: 12-12-12} ]}";
        Stock infyStock = new Stock("Infy", 23.45, "11-12-12", "2");
        Stock twStock = new Stock("TW", 43.45, "12-12-12","3");

        APIResponse apiResponse = new APIResponse();
        apiResponse.setStatus("SUCCESS");
        apiResponse.setResponseText(jsonResponse);

        ViewProfileResponse response = new ViewProfileResponse();
        response.setParameters(apiResponse);

        String name = response.getName();
        Stock[] stocks = response.getStocks();

        Assert.assertThat(response.isSuccess(), is(true));
        Assert.assertThat(name, is("Dude"));
        Assert.assertThat(stocks.length, is(2));
        Assert.assertTrue(String.format("Expected: %s, but got %s ", infyStock, stocks[0]), stocks[0].equals(infyStock));
        Assert.assertTrue(String.format("Expected: %s, but got %s ", twStock, stocks[1]), stocks[1].equals(twStock));
    }

    @Test
    public void constructFailureResponseForAnEmptyApiResponse(){
        APIResponse apiResponse = new APIResponse();
        ViewProfileResponse response = new ViewProfileResponse();
        response.setParameters(apiResponse);

        Assert.assertThat(response.isSuccess(), is(false));
    }
}
